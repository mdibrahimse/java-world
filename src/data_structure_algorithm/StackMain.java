
package data_structure_algorithm;
import java.io.IOException;

public class StackMain {
    public static void main(String[] args){
        try{
            Stack obj = new Stack(3);
            obj.push(10);
            obj.push(4);
            obj.push(13);
            obj.display();
            obj.pop();
            obj.display();
        }catch(Exception e){
            e.printStackTrace();
        }
        
    }
}
