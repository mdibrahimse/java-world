
package data_structure_algorithm;

public class LinearSearch {
    public static void main(String arg[]){
        int arrNumber[] = {2,5,7,8,9,11,26,47,59,61,77,79,82};
        int item = 70;
        int size=arrNumber.length;
        int i;
        for(i=0; i<size; i++){
            if(arrNumber[i] == item){
                System.out.println("Item is Found in : "+i);
                break;
            }
        }
        if(i==size){
            System.out.println("Item not found");
        }
    }
}
