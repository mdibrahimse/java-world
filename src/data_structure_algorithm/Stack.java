
package data_structure_algorithm;


public class Stack {
    private int[] arr;
    private int top,size,capacity;
    
    public Stack(int capacity) throws Exception{
        if(capacity  < 1){
            throw new Exception("Size of Stack can't be smaller than 1");
        }
        this.capacity = capacity;
        arr = new int[capacity];
        size = 0;
        top = -1;
    }
    public boolean isFull(){
        if(size == capacity){
            return true;
        }else{
            return false;
        }
    }
    public boolean isEmpty(){
        if(size == 0){
            return true;
        }else{
            return false;
        }
    }
    public void display(){
        if(isEmpty()){
            throw new ArrayIndexOutOfBoundsException("Stack is empty");
        }else{
            System.out.println("Stack:\t");
            for(int i=0; i<=top; i++){
                System.out.print(arr[i]+"\t");
            }
            System.out.println();
        }
    }
    public void push(int value){
        if(isFull()){
            throw new ArrayIndexOutOfBoundsException("Array Overflow!!!");
        }else{
            size++;
            top++;
            arr[top] = value;
        }
    }
    public void pop() throws StackException{
        if(isEmpty()){
            throw new StackException();
        }else{
            size--;
            top--;
        }
    }
}
