
package data_structure_algorithm;


public class BinarySearch {
    public static void main(String arg[]){
        int arrNumber[] = {2,3,6,7,9,22,56,78,99,100,103};
        int item,startpoint,endpoint,midpoint,location,comparison;
        item = 100;
        startpoint = 0;
        endpoint = arrNumber.length-1;
        location = -1;
        comparison = 0;
        
        while(startpoint<=endpoint){
            comparison++;
            midpoint = (startpoint+endpoint)/2;
            
            if(arrNumber[midpoint]==item){
              location = midpoint;
              break;
            }else if(arrNumber[midpoint]<item){
                startpoint = midpoint+1;
            }else{
                endpoint = midpoint-1;
            }
            
        }
        
        System.out.println("Given a sorted array of 9 elemnets");
            for(int i:arrNumber){
                System.out.print(" "+i);
            }
            System.out.println("");
            if(location==-1){
                System.out.println("not found "+item+" "+comparison+" Total number of comparison");
            }else{
                System.out.println(item+" found"+" in "+location+" "+comparison+" Total number of comparison");
            }
    }
}
