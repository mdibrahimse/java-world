/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package basicjava;

/**
 *
 * @author Solvicor
 */
public class ArrayDetails {
    public static void main(String arg[]){
        //decleare array
        String car[] = {"volvo","audi","BMW"};
        int myNumber[][] = {{1,2,3,4},{5,6,7,8}}; 
        
        //Printing array
        for(String i:car){
            System.out.println(i);
        }
        //Array Length
        System.out.println("Array length is : "+car.length);
        
        //update araay specific index
        System.out.println("New updated list");
        car[0] = "Ferari";
        for(String i:car){
            System.out.print(i+",");
        }
        
        //multidimentional array
        System.out.println("\n");
        System.out.print("This is Multidimentional array");
        for(int i=0; i<myNumber.length; i++){
            System.out.println("");
            for(int j=0; j<myNumber[i].length; j++){
                System.out.print(myNumber[i][j]);
            }
        }
    }
}
